[TOC]
# Autonium #
Idea behind Autonium is to simplify Automation scripts development by providing Wrappers in place of multiple similar methods.

## About Autonium ##
* Autonium is a test automation framework for web applications which uses Java and Selenium. 
* It uses Maven project structure. 
* Easy to understand and use it. 
* Having basic idea on Java, Selenium, Maven and TestNG is good enough to start with Autonium.

## Why Autonium? ##
* Framework provides single wrappers for common methods like Find Element, Drop down selection, Wait.
* No more dependency on multiple methods for drop down selection, finding elements on webpage etc.

**Selenium Web driver code:**

```
#!java
    driver.findElement(By.id("continents"));
    driver.findElement(By.xpath("//select[@class='cardNumber']"));

```
**Autonium code:**

```
#!java

    driver.findElement("id:continents");
    driver.findElement("xpath://select[@class='cardNumber']");
```

* Users can use both Webdriver methods and Autonium methods.
* Provides support to all major browsers.
* Capability of executing scripts on Remote Machines.

## Pre-Requisites ##
* Basic knowledge on [Java](http://www.tutorialspoint.com/java/), [Selenium](http://www.guru99.com/selenium-tutorial.html), [Maven](http://www.tutorialspoint.com/maven/) and [TestNG](http://www.tutorialspoint.com/testng/index.htm)
* [Download and Install Java](https://java.com/en/download/)
* [Download Maven](https://maven.apache.org/download.cgi) and [Install Maven](https://maven.apache.org/install.html)
* [Download and Install eclipse](https://eclipse.org/downloads/) - you can use other IDE's like IntelliJ IDEA

##  Setup ##
* Make sure [Java](http://www.kingluddite.com/tools/how-do-i-add-java-to-my-windows-path), [Maven](http://www.mkyong.com/maven/how-to-install-maven-in-windows/) variables are added to path.
* Open eclipse, create a Maven Project - [help](http://www.tech-recipes.com/rx/39279/create-a-new-maven-project-in-eclipse/)
* After creating Maven project, folder structure looks as shown in image below 

    ![MavenProject.jpg](https://bitbucket.org/repo/bko6zz/images/4054546316-MavenProject.jpg)
 
* Open [pom.xml](http://www.tutorialspoint.com/maven/maven_pom.htm) and add dependency for **Autonium**, **Selenium** and **TestNG**
    * Selenium    
```
#!XML
        <dependency>
            <groupId>org.seleniumhq.selenium</groupId>
            <artifactId>selenium-java</artifactId>
            <version>2.53.0</version>
        </dependency> 
```
* * TestNG
```
#!XML
        <dependency>
            <groupId>org.testng</groupId>
            <artifactId>testng</artifactId>
            <version>6.8</version>
            <scope>test</scope>
        </dependency>
```
* * Autonium
```
#!XML

        <dependency>
            <groupId>org.selenium</groupId>
            <artifactId>Autonium</artifactId>
            <version>1.2</version>
        </dependency>
```
* Create **config.properties** file in **resources** folder

    ![ConfigFile_Creation.jpg](https://bitbucket.org/repo/bko6zz/images/2936300160-ConfigFile_Creation.jpg)

* Create **testdata** and **identifiers** folders in **resources** folder

    ![testdataandidentifiers_Folders.jpg](https://bitbucket.org/repo/bko6zz/images/2561441351-testdataandidentifiers_Folders.jpg)

* Rename **src/test/java** folder to **src/test/xml**. Then remove the package and java file present in **src/test/xml** folder.

    ![src-test-xml_folder.jpg](https://bitbucket.org/repo/bko6zz/images/3343022662-src-test-xml_folder.jpg)

## Config Properties ##
* Create config file under Resources folder and this file is mandatory to run test scripts. 
* Config file should be named as "config.properties"
* Values can be entered in config file as mentioned below.

```
#!Properties
    BASE_URL = https://bitbucket.org/
    Environment = q1
    Browser = 
    SeleniumServer =
    SeleniumPort =  
```
* BASE_URL, Environment, Browser, SeleniumServer and SeleniumPort are default fields for config.properties file.
* Tests cannot be run without these fields in config file. Except BASE_URL all fields can be empty.
* BASE_URL is mandatory and value should be provided in config file.
* Accepted Browser values

```
#!java

    FIREFOX           - firefox or ff
    GOOGLE CHROME     - chrome or googlechrome or gc
    INTERNET EXPLORER - ie or iehta or internetexplorer or iexplore
    SAFARI            - safari
```

* Default browser will be Firefox (if value is not given in config file)
* ** Environment ** value is used to get test data from properties files in testdata folder.
* ** SeleniumServer ** is an IP Address or Host Name of remote machine where test scripts can be executed (Selenium Stand alone server should be running on remote machine).
* ** SeleniumPort ** is port on which selenium server is running. If Selenium Port is not provided by default it will be set to **4444**.

## Test Data Files ##
* Create Test data folder under resources folder in your project. Name should be "testdata".
* Test data folder is not required to run scripts unless the data is read from test data files.
* To store any test data create a .properties file and save key/value pairs in test data file.
* Test data file name should be <environment>.properties

```
#!java

Sample Test data file names:

q1.properties
q2.properties
staging.properties
live.properties
```

* Syntax for test data values can be as given below in q1.properties file:
```
#!Properties
    username = admin
    password = admin
```
* To read value from Test data use the following method:

```
#!java
    public String getTestData(String propertyName) {
               ....
     }
```
* This method returns parameter value as String from <environment>.properties test data file.
```
#!java
    driver.getTestData("username");
```
* When above method is called, username value from q1.properties file will be returned.
* Test case will fail  with respective error message if file is not found or property not listed in test data file.

## Identifiers Files ##
* Create a "identifiers" folder under resources directory to maintain files for storing Identifier values.
* Identifiers folder is not mandatory to run scripts unless identifier values are read from identifier files.
* Create a .properties file and store key/value pairs for identifiers.
* Syntax for values in identifiers file is given below:

```
#!Properties
    loginLink = //a[text()='Log In']
    searchButton = id:search
```
* To read values from Identifiers file use following method:
```
#!java
    public String getIdentifierData(String fileName, String propertyName) {
            ...
    }

```
* Above method returns value of identifier from file based on argument passed to it.
```
#!java

    driver.getIdentifierData("homepage", "loginLink");
```
* After calling above method, loginLink value from homepage.properties file in identifiers folder will be returned.
* Appropriate error will be shown while trying to read values from identifier file which does not exist.

## Writing Scripts ##
1. Import Autonium class.
2. Import TestNG classes.
3. Instantiate Autonium object.
4. Write methods.

Sample code to search for "Selenium" on Google:

```
#!java
    //importing Autonium class
    import org.selenium.Autonium.Autonium;

    //importing testng classes
    import org.testng.annotations.AfterClass;
    import org.testng.annotations.Test;

    public class App 
    {
        //Declaring Autonium object
        Autonium driver;
	
        //Test Case
        @Test
        public void v()
        {
            //Initializing Autonium object
            driver = new Autonium();

            //Calling Webdriver Method using Autonium Object
            driver.AutoniumDriver.get("http://www.google.com");
            driver.AutoniumDriver.manage().window().maximize();

            //Autonium Wrapper method
            driver.findElement("name:q").sendKeys("Selenium");
            driver.findElement("//button[@value='Search']/span").click();
        }
	
        @AfterClass
        public void closeDriver() {
            driver.AutoniumDriver.quit();
        }
    }
```
## Running Scripts ##
* Create a **XML** file in **src/test/xml** folder

    ![src-test-xml_file.jpg](https://bitbucket.org/repo/bko6zz/images/2454151344-src-test-xml_file.jpg)

* Include the class to execute in created xml file - Should use Qualified Class Name

```
#!XML

    <!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd" >
    <suite name = "sample">
        <test name = "sample1">
            <classes>
                <class name="org.Autonium.qa.Autonium_sample.App"/>
            </classes>
        </test>
    </suite>
```
* Add below code in your pom.xml

```
#!XML

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.19.1</version>
                <configuration>
                    <suiteXmlFiles>
                        <suiteXmlFile>src\test\xml\testng.xml</suiteXmlFile>
                    </suiteXmlFiles>
                </configuration>
            </plugin>
        </plugins>
    </build>
```



### Running Tests From Eclipse ###
* Right click on the **pom.xml** select **Maven Install** from **Run As**
    
    ![Run_Eclipse.jpg](https://bitbucket.org/repo/bko6zz/images/1507841983-Run_Eclipse.jpg)

### Running Tests From Windows Command Prompt ###
* Open Command Prompt

    ![Run_cmd1.jpg](https://bitbucket.org/repo/bko6zz/images/3001028273-Run_cmd1.jpg)

* Navigate to Project Folder

    ![Run_cmd2.jpg](https://bitbucket.org/repo/bko6zz/images/1179051400-Run_cmd2.jpg)
* Running without arguments

```
#!CMD

    mvn install
```
* Running with arguments


```
#!CMD

    mvn install -DBrowser=gc
    mvn install -DSeleniumServer=127.0.0.1 [Use Remote Machine IP or Machine Name]
    mvn install -DSeleniumPort=4444
    mvn install -DBASE_URL=http://www.google.com

# Running with multiple arguments
    mvn install -DBrowser=ie -DSeleniumServer=127.0.0.1 -DBASE_URL=http://www.google.com
```

## Autonium Methods ##
### Get Test Data ###
* This method returns value of given property (key) from environment file present in "**src/main/resources/testdata**" folder.
* Fails the test case if File not present in testdata folder or Property not present in file.
* Method Signature

```
#!java

    public String getTestData(String propertyName) {
        ...
    }
```
* For more details [click here](#markdown-header-test-data-files)

### Get Identifier Data ###
* This method returns value of given property (key) from given identifiers file present in "**src/main/resources/identifiers**" folder.
* Fails the test case if File not present in testdata folder or Property not present in file.
* Method Signature

```
#!java

    public String getIdentifierData(String fileName, String propertyName) {
        ...
    }
```
* For more details [click here](#markdown-header-identifiers-files)

### Find Element ###
* This method returns corresponding WebElement for the identifier passed as an argument.
* Fails the test case if element not found.
* Method Signature
```
#!java
    public WebElement findElement(String identifier){
            ...
    }
```
* Allowed arguments for this method are :

```
#!java

    id:    Element Id
    cn:    Element Class Name
    css:   Element CSS Selector
    name:  Element name
    lt:    Element Link Text
    plt:   Element Partial Link Text
    tname: Element Tag Name
    xpath: Element XPath. For xpath, also can use directly element xpath without "xpath:"
```
* Method usage with allowed arguments:

```
#!java
    //To find element based on Id
    driver.findElement("id:username");

    //To find element based on xpath
    driver.findElement("//input[@id='username']");

    //To find element based on Class Name
    driver.findElement("cn:login");

    //To find element based on CSS Selector
    driver.findElement("css:#username");

    //To find element based on Name Attribute
    driver.findElement("name:login");

    //To find element based on Link Text
    driver.findElement("lt:Login Link");

    //To find element based on Partial Link Text
    driver.findElement("plt:Login");

    //To find element based on Tag Name
    driver.findElement("tname:a");
```

### Find Elements ###
* This method returns list of WebElements for the identifier passed as an argument.
* Fails the test case if element not found.
* Method Signature
```
#!java
    public List<WebElement> findElements(String identifier) {
            ...
    }
```
* Allowed arguments for this method are :

```
#!java

    id:    Element Id
    cn:    Element Class Name
    css:   Element CSS Selector
    name:  Element name
    lt:    Element Link Text
    plt:   Element Partial Link Text
    tname: Element Tag Name
    xpath: Element XPath. For xpath, also can use directly element xpath without "xpath:"
```
* Method usage with allowed arguments:

```
#!java
    //To get elements list based on Id
    driver.findElements("id:username");

    //To get elements list based on xpath
    driver.findElements("//input[@id='username']");

    //To get elements list based on Class Name
    driver.findElements("cn:login");

    //To get elements list based on CSS Selector
    driver.findElements("css:#username");

    //To get elements list based on Name Attribute
    driver.findElements("name:login");

    //To get elements list based on Link Text
    driver.findElements("lt:Login Link");

    //To get elements list based on Partial Link Text
    driver.findElements("plt:Login");

    //To get elements list based on Tag Name
    driver.findElements("tname:a");
```

### Select From Drop Down ###
* This method selects value from drop down based on index/value/visible text.
* Fails the test case if element or SelectValue is not found
* Method Signature

```
#!java

    public void selectFromDropDown(String identifier, String selectValue) {
        ...
    }
```
* Allowed arguments for identifier are same as for Find Element method.
* Allowed arguments for value are :
```
#!java
    value: Value to be selected from drop down
    vtext: Visible text to be selected from drop down
    index: Index to be selected from drop down
```
* Method usage with allowed arguments
```
#!java

    //To select from drop down based on index
    driver.selectFromDropDown("id:continents","index:1");

    //To select from drop down based on value
    driver.selectFromDropDown("id:continents","value:Asia");

    //To select from drop down based on visible text
    driver.selectFromDropDown("id:continents","vtext:Europe");

```

### Deselect From Drop Down ###
* This method deselects value from select based on index/value/visible text.
* Fails the test case if element or DeSelectValue is not found
* Method Signature

```
#!java

    public void deSelectFromDropDown(String identifier, String deSelectValue){
        ...
    }
```
* Allowed arguments for identifier are same as for Find Element method.
* Allowed arguments for value are :
```
#!java
    value: Value to be deselected from select
    vtext: Visible text to be deselected from select
    index: Index to be deselected from select
```
* Method usage with allowed arguments
```
#!java

    //To deselect from select based on index
    driver.deSelectFromDropDown("id:continents","index:1");

    //To deselect from select based on value
    driver.deSelectFromDropDown("id:continents","value:Asia");

    //To deselect from select based on visible text
    driver.deSelectFromDropDown("id:continents","vtext:Europe");

```

### Wait For Element Presence ###
* Waits until element specified with identifier appears on current page.
* Fails if timeout expires before the element appears.
* Method signature

```
#!java

    public void waitForElementPresence(String identifier, int timeOutInSeconds) {
        ...
    }
```
* Allowed arguments for identifier are same as for Find Element method.
* timeOutInSeconds is integer value.
* Method usage with allowed arguments

```
#!java
    //Waits 60 seconds for "continents" on web page
    driver.waitForElementPresence("id:continents", 60);

```


### Wait For Element Visible ###
* Waits until element specified with identifier is visible.
* Fails if timeout expires before the element is visible.
* Method signature

```
#!java

    public void waitForElementVisisble(String identifier, int timeOutInSeconds) {
        ...
    }
```
* Allowed arguments for identifier are same as for Find Element method.
* timeOutInSeconds is integer value.
* Method usage with allowed arguments

```
#!java
    //Waits 60 seconds for "continents" visibility on web page
    driver.waitForElementVisisble("id:continents", 60);

```

### Get Http Status Code ###
* Fetches Http/Https status code for the required request.
* Fails if the URL is not provided correctly or URL cannot be reached.
* Method signature

```
#!java

    public int getHttpStatusCode(String urlString){
     ...
    }
```
* Allowed values for urlString are:

```
#!java

https://autonium.wordpress.com
http://www.seleniumhq.org/
```
* Method usage with allowed arguments

```
#!java
//Returns Http status code for given urlString
driver.getHttpStatusCode("https://autonium.wordpress.com");
```

### Mouse Hover ###
* Performs Mouse hover action over given element in web page.
* Fails if the element is not visible/displayed on the page.
* Method signature

```
#!java

public void mouseHover(String identifier){
     ...
}
```

* Allowed arguments for identifier are same as for Find Element method.
* Method usage with allowed arguments

```
#!java
//Hovers mouse over element given
driver.mouseHover("cn:ui-link");
```
### Drag And Drop ###
* Performs Drag and drop action from one location to another location in a web page.
* Fails if the element is not visible/displayed on the page.
* Method signature

```
#!java

public void dragAndDrop(String source, String target){
     ...
}
```

* Allowed arguments for identifier are same as for Find Element method.
* Method usage with allowed arguments

```
#!java

//Drags and drops source element at target element
driver.dragAndDrop("//div[@id='treebox1']//span[text()='Thrillers']", "//div[@id='treebox2']//span[text()='Bestsellers']");
```

### Double Click ###
* Performs Double Click action over given element in web page.
* Fails if the element is not visible/displayed on the page.
* Method signature

```
#!java

public void doubleClick(String identifier){
     ...
}
```

* Allowed arguments for identifier are same as for Find Element method.
* Method usage with allowed arguments

```
#!java

//Performs double click on given element
driver.doubleClick("id:submit");
```

### Take Screenshot ###
* This method takes Screenshot and stores in Screenshots folder of workspace.
* Fails the test case if Screenshots folder cannot be created or Screenshot cannot be captured.
* Method signature

```
#!java

public void takeScreenshot() {
    ...
}
```
* Method usage with allowed arguments

```
#!java

//Takes Screenshot and stores it in Screenshots folder of workspace
driver.takeScreenshot();
```

### Take Screenshot with Required File Name ###
* This method takes screenshot with given file name and stores it in Screenshots folder of workspace.
* Fails the test case if Screenshots folder cannot be created, Screenshot cannot be taken, File name is empty or File name is given with extension.
* Method signature

```
#!java

public void takeScreenshot(String fileName) {
    ...
}
```
* Any valid text can be given as fileName and it should not contain any extension.
* Method usage with allowed arguments

```
#!java

//Takes screenshot and stores it as homepage.png in Screenshots folder.
driver.takeScreenshot("homepage");
```

### Take Screenshot with Required File Name and Directory ###
* This method takes screenshot with given file name and stores it in given directory.
* Fails the test case if given directory cannot be created, Screenshot cannot be taken, File name/Directory path is empty or File name is given with extension.
* Method signature

```
#!java

public void takeScreenshot(String fileName, String dirPath) {
    ...
}
```
* Any valid text can be given as fileName and it should not have any extension.
* Any valid directory path can be given as dirPath.
* Method usage with allowed arguments

```
#!java
//Takes screenshot and stores it as homepage.png in D:\Images path.
driver.takeScreenshot("homepage","D:\\Images");
```
## Who do I talk to? ##

* [Vijay Kumar Reddy Gandra](https://bitbucket.org/vijay1437143/)
* [Venkat Ramana Reddy Parine](https://bitbucket.org/vparine/)