package org.autonium.qa.autonium_sample;

import org.selenium.autonium.Autonium;
import org.testng.annotations.Test;


public class WithoutDataFiles {
	
	private Autonium driver;

	@Test
	public void selectFromDropDown() {
		
		//Initializing driver variable
		driver = new Autonium();
		
		//driver loads the BASE_URL provided in config file.
		
		//Maximize browser window.
		driver.autoniumDriver.manage().window().maximize();
		
		//Finding element using id
		driver.findElement("id:exp-2");
		
		//Selecting an element from drop down
		driver.selectFromDropDown("name:continents", "vtext:Africa");
		
		//Closing the driver instance
		driver.autoniumDriver.quit();
	}

}

