package org.autonium.qa.autonium_sample;

import org.selenium.autonium.Autonium;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class WithDataFiles {
	
	private Autonium driver = new Autonium();

	@Test
	public void enterText() {
				
		//Maximizing browser window
		driver.autoniumDriver.manage().window().maximize();
		
		//First Name value will be fetched from test data file.
		//Test data file will be chosen based on Environment given in config file.
		String firstName =  driver.getTestData("firstName");
		
		//First Name locator value from identifiers file will be returned.
		String firstNameLocator = driver.getIdentifierData("locators", "firstName");
		
		//Entering firstName value into firstNameLocator
		driver.findElement(firstNameLocator).sendKeys(firstName);
	}
	
	@Test
	public void selectFromDropDown() {
		
		//Selecting Australia from Continents
		driver.selectFromDropDown(driver.getIdentifierData("locators", "continents"), driver.getTestData("continent_name"));
	}
	
	@Test
	public void testStatusCodes() {
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/200"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/201"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/202"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/203"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/204"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/205"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/206"));
		
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/300"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/301"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/302"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/303"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/304"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/305"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/306"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/307"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/308"));
		
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/400"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/401"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/402"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/403"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/404"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/405"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/406"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/407"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/408"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/409"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/410"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/411"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/412"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/413"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/414"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/415"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/416"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/417"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/418"));
		
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/422"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/428"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/429"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/431"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/451"));
		
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/500"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/501"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/502"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/503"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/504"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/505"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/511"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/520"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/522"));
		System.out.println(driver.getHttpStatusCode("http://httpstat.us/524"));
	}

	@AfterClass
	public void closeDriver() {

		//Closing driver
		driver.autoniumDriver.quit();
	}
}