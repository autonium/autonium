package org.selenium.autonium.loadproperties;

import java.io.InputStream;
import java.util.Properties;

import org.selenium.autonium.results.Logger;

/**
	 * This file for loading properties file. <p>
	 * @author   Vijay Kumar Reddy Gandra <p>
	 * @since    1.0
*/

public class LoadProperties {
	
	
	private Properties dataProperties = new Properties();

	/**
	 * Returns value of given property (key) from given testdata file present in given folder.
	 *  <p>
	 * @param    classLoader 
	 * 				- ClassLoader for the class
	 * @param	 sFileName
	 * 				- File Name from which property has to read
	 * @param	 sFolderName
	 * 				- Folder Name from which File has to read
	 * @param	 sPropertyName
	 * 				- Property Name for which we need to read value
	 * <p>
	 * @return   Returns the value of given property.
	 * <p>
	 * @author   Vijay Kumar Reddy Gandra
	 * <p>
	 * @since    1.0
	 */
	
	public String getPropertFromFile(ClassLoader classLoader, String sFileName, String sFolderName, String sPropertyName) {
		
		String sTestdataFullName = sFolderName+"/"+sFileName;
		try {
			InputStream inputstream = classLoader.getResourceAsStream(sTestdataFullName);
			dataProperties.load(inputstream);
			inputstream.close();
		}
		catch(Exception e) {
			Logger.error("Failed to load "+ sFileName +" with below Reason:\n" + e.getMessage());
		}
		
		return LoadConfigProperties.getProperty(dataProperties,sFileName,sPropertyName);
	}
}
