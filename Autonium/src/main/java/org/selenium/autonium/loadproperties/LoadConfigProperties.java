package org.selenium.autonium.loadproperties;


import java.io.InputStream;
import java.util.Properties;

import org.selenium.autonium.results.Logger;
   
   /**
     * This file for initializing values specified in config.properties file.
     * User must initialize the values in config.properties file to run the test
     * This file for initializing values specified in RunConfig.properties file.
     * User must initialize the values in RunConfig.properties file to run the test
     * on desired environment, machine name, browser combinations.
     * 
     * @author   Vijay Kumar Reddy Gandra
	 * @since    1.0
	 */

public class LoadConfigProperties {
	
     public String browserType;
     public String baseurl;
     public String seleniumServer;
     public String seleniumServerPort;
     public String seleniumPortDefault = "4444";
     public String environment;
     private Properties configProperties = new Properties();
    
    public LoadConfigProperties(ClassLoader classLoader) {
        
        try {
             InputStream inputstream = classLoader.getResourceAsStream("config.properties");
            configProperties.load(inputstream);
            inputstream.close();
        }

		catch(Exception e) {
			Logger.error("Failed with below Reason:\n" + e.getMessage());
		}
		
        loadConfig(configProperties,classLoader);
    }
    
     /**
      * Loads the config.properties file. Loads Browser, SeleniumServer, SeleniumPort, BASE_URL and ENVIRONMENT variables. 
      * <p>
      * @param    configProperties
      * @param	  classLoader
      * <p>
      * @author   Vijay Kumar Reddy Gandra
      * @author   Venkat Ramana Reddy Parine
      * <p>
      * @since    1.0
      */
     
     private void loadConfig(Properties configProperties, ClassLoader classLoader) {
         Logger.info("/*** Reading from Config file ***/");
         
         /**
          * Reading Browser value from config file.
          */
         browserType = getProperty(configProperties, "Config", "Browser");
         if(browserType == null || browserType.isEmpty())
        	 Logger.warning("Browser value is not provided as argument and is not provided in Config file. By default intializing browser to FIREFOX.");
         
         /**
          * Reading SeleniumServer value from config file.
          */
         seleniumServer = getProperty(configProperties, "Config", "SeleniumServer");
         if(seleniumServer == null || seleniumServer.isEmpty())
        	 Logger.warning("Selenium Server is not provided as argument and is not provided in Config file.");
         
         /**
          * Reading SeleniumPort value from config file.
          */
         seleniumServerPort = getProperty(configProperties, "Config", "SeleniumPort");
         if(seleniumServerPort == null || seleniumServerPort.isEmpty()) {
        	 Logger.warning("Selenium Server Port is not provided as argument and is not provided in Config file.");
        	 if(seleniumServer != null && !seleniumServer.isEmpty()) {
            	 Logger.info("Intializing the Selenium Server Port to 4444. As Server value is provided but port is not provided");
            	 seleniumServerPort = seleniumPortDefault;
        	 }
         }
         else 
         {
        	 if(seleniumServer == null || seleniumServer.isEmpty()) {
            	 Logger.info("There is no meaning in providing Selenium Server Port without Selenium Server. As Selenium Server is not provided test cases will execute locally.");
            	 seleniumServerPort = seleniumPortDefault;
        	 }
         }
         
         /**
          * Reading BASE_URL value from config file.
          */
         baseurl = getProperty(configProperties, "Config", "BASE_URL");
         if(baseurl == null || baseurl.isEmpty())
        	 Logger.error("Base URL is not provided as argument and is not provided in Config File. Base URL should be given in Config file or as command line argument.");
         
         /**
          * Reading ENVIRONMENT value from config file.
          */
         environment = getProperty(configProperties, "Config", "Environment");
         if(environment == null || environment.isEmpty())
        	 Logger.info("Environment is not provided as argument and is not provided in Config File. Tests will run without using test data property files.");
     }
     
     /**
      * Returns value of given property (key) from command line arguments or given properties.
      * <p>
      * @param    props
      * @param	  fileName 
      * @param	  key
      * <p>
      * @return   Returns propertyValue from given properties & key.
      * <p>
      * @author   Vijay Kumar Reddy Gandra
      * <p>
      * @since    1.0
      */
     
     static String getProperty(Properties props, String fileName, String key) {
         
    	 String sReturn = "";
    	 /**
    	  * Loading property value from Command Prompt.
    	  **/
    	 sReturn = System.getProperty(key);
 		if(sReturn != null && !sReturn.equals("${"+key+"}") && sReturn.length() != 0){
 			Logger.info(key + " from argument is: "+sReturn);
 			return sReturn;
 		}
 		else if(props.containsKey(key)) {
 			sReturn = props.getProperty(key);
 			if(sReturn != null && !sReturn.equals("${"+key+"}") && sReturn.length() != 0)
 				Logger.info(key + " value from "+ fileName +" file is: "+sReturn);
 			return sReturn;
 		}
         else
             Logger.error(key+" is not provided as argument and is not provided in properties file. Please cross check variable spellings.");
         return key;
     }
     
}
