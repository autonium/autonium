package org.selenium.autonium.results;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.testng.Reporter;

/**
 * This class will print the log to console 
 * @author   Vijay Kumar Reddy Gandra
 * @since    1.0 
 */

public class Logger {
	
	/**
	 * Fails the test case and prints the given error message.
	 * <p>
	 * @param	sMSG
	 * 			  - Error Message
	 * <p>
	 * @see		#warning(String)
	 * @see		#info(String)
	 * @author 	Vijay Kumar Reddy Gandra 
	 * <p>
	 * @since	1.0
	 */
    public static void error(String sMSG)
    {
        org.testng.Assert.fail(sMSG);
    }

	/**
	 * Prints the given warning message.
	 * <p>
	 * @param	sMSG
	 * 			  - Warning Message
	 * <p>
	 * @see		#error(String)
	 * @see		#info(String) 
	 * @author 	Vijay Kumar Reddy Gandra 
	 * <p>
	 * @since	1.0
	 */
     public static void warning(String sMSG) {
         log("[ WARNING ] : "+sMSG, true);     
    }
    
 	/**
 	 * Prints the given information message. 
 	 * @param	sMSG
 	 * 			  - Info Message
 	 * <p>
 	 * @see		#error(String)
 	 * @see		#warning(String) 
 	 * @author 	Vijay Kumar Reddy Gandra 
 	 * <p>
 	 * @since	1.0
 	 */
    public static void info(String sMSG) {
         log("[ INFO ]    : "+sMSG, true );
    }
    
    public static void action() {
		
	}
	
    /**
     * Actual method to print the message to console. 
     * @param 	sMSG
     * @param 	logToStandardOut 
     * @author 	Vijay Kumar Reddy Gandra 
 	 * @since	1.0
     */
	private static void log(String sMSG, boolean logToStandardOut) {
		
		//String sThreadId = String.format("(%04d)", Thread.currentThread().getId());
		//prefix date stamp
		//sMSG = getFormattedDate("dd-MM-yyyy HH:mm:ss") + " "+sThreadId+" :"+sMSG;
		sMSG = getFormattedDate("dd-MM-yyyy HH:mm:ss") + " : "+sMSG;
		
		Reporter.log(sMSG, logToStandardOut);
	}
	
	/**
	 * This method gives the formatted date time. 
	 * <p>
	 * @author Vijay Kumar Reddy Gandra 
	 * <p>
	 * @param sFormat 
	 * <p>
	 * @return Returns formatted date.
	 * <p>
	 * @since 1.0 
	 */
	
	public static String getFormattedDate(String sFormat) {
		String timestamp = "";
		try{
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat dateFormat = new SimpleDateFormat(sFormat);
			timestamp = dateFormat.format(cal.getTime());
		}catch(Exception e){}
		return timestamp;
	}
	
	/**
	 * This method Fails the test case with proper error message when UnSupported identifier was give by user. 
	 * <p>
	 * @author Vijay Kumar Reddy Gandra
	 * @param identifierType
	 * <p>
	 * @since 1.0 
	 * @see Logger#UnsuppportedSelectorErrorMsg(String)
	 */
	
	public static void UnsuppportedIdentifierErrorMsg(String identifierType) {
		
		Logger.error("Unsupported Identifier Type: "+ identifierType +"\n"
							+ "			  Supported Identifiers: \n"
							+ "				1. CLASSNAME - cn \n"
							+ "				2. CSSSELECTOR - css \n"
							+ "				3. ID - id \n"
							+ "				4. LINKTEXT - lt \n"
							+ "				5. NAME - name \n"
							+ "				6. PARTIALLINKTEXT - plt \n"
							+ "				7. TAGNAME - tname \n"
							+ "				8. XPATH - xpath \n"
							+ "				Eg: tname:elementtagname\n");
		
	}
	
	/**
	 * This method Fails the test case with proper error message when UnSupported select type was give by user. 
	 * <p>
	 * @author Vijay Kumar Reddy Gandra
	 * @param selectValue
	 * <p>
	 * @since 1.0 
	 * @see #UnsuppportedIdentifierErrorMsg(String)
	 */
	
	public static void UnsuppportedSelectorErrorMsg(String selectValue) {
		
		Logger.error("Unsupported Selector/Deselector Type: "+ selectValue +"\n"
							+ "			  Supported  Selector/Deselector Types : \n"
							+ "				1. Visible Text - vtext\n"
							+ "				2. Value - value\n"
							+ "				3. Index - index\n"
							+ "				Eg: vtext:VisibleText\n");
		
	}
}