package org.selenium.autonium.utils;

import org.openqa.selenium.remote.BrowserType;

/**
 * This class is used to define all browser types. <p>  
 * @author   Vijay Kumar Reddy Gandra <p>
 * @since    1.0
 */

public class BrowserTypeExtended implements BrowserType {

	public static final String HTMLUNIT = "htmlunit";
	public static final String OTHER 	= "other";
	private static String browserCommand = "";
    public static String sUnsuppportedBrowserErrorMsg = "	Unsupported browser: "+ browserCommand + "\n" +
			"				Supported Browsers List: \n" +
			"				FIREFOX			- firefox or ff\n" +
			"				GOOGLE CHROME		- chrome or googlechrome or gc\n" +
			"				INTERNET EXPLORER	- ie or iehta or internetexplorer or iexplore\n" +
			"				HTMLUNIT		- htmlunit \n" +
			"				SAFARI			- safari \n";
    
    /**
     * Returns Browser Name.
     * 
     * @param 	sBrowserCommand
     * 				- Value from Command line or Config file
     * @return 	Returns Browser Name
     * @author   Vijay Kumar Reddy Gandra
     * @since    1.0
     */
	public static String getBrowserType(String sBrowserCommand)
	{
		browserCommand = sBrowserCommand;
		String firefox[] = {"firefox" , "ff"};
		String ie[]		 = {"internetexplorer" , "ie" , "iehta" , "iexplore" , "iexplorer"};
		String chrome[]  = {"chrome" , "googlechrome", "gc"};
		String htmlunit[]= {"htmlunit"};
		String safari[]	 = {"safari"};

		//firefox
		for (String sBrowser : firefox)
		{
			if (sBrowserCommand.equalsIgnoreCase(sBrowser) || sBrowserCommand.isEmpty())
				return FIREFOX;
		}

		//chrome
		for (String sBrowser : chrome)
		{
			if (sBrowserCommand.equalsIgnoreCase(sBrowser))
				return CHROME;
		}
		
		//ie
		for (String sBrowser : ie)
		{
			if (sBrowserCommand.equalsIgnoreCase(sBrowser))
				return IEXPLORE;
		}

		//htmlunit
		for (String sBrowser : htmlunit)
		{
			if (sBrowserCommand.equalsIgnoreCase(sBrowser))
				return HTMLUNIT;
		}
		
		//safari
		for(String sBrowser : safari) {
			if(sBrowserCommand.equalsIgnoreCase(sBrowser))
				return SAFARI;
		}
		
		return OTHER;
	}
}