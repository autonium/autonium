package org.selenium.autonium;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.selenium.autonium.loadproperties.LoadConfigProperties;
import org.selenium.autonium.loadproperties.LoadProperties;
import org.selenium.autonium.results.Logger;
import org.selenium.autonium.utils.BrowserTypeExtended;

/**
 * This class is heart of the framework 
 * All actions will start from this call
 * @author   Vijay Kumar Reddy Gandra
 * @author   Venkat Ramana Reddy Parine
 * @since    1.0
 */

public class Autonium{
	
	/***
	 * WebDriver Reference.
	 * <p>
	 * Using this reference can access all Webdriver Methods.
	 ***/
	
	public WebDriver autoniumDriver;
	
	/***
	 * Creating References for LoadConfigProperties, LoadProperties
	 * Creating Variables sBrowserType, classLoader, testDataFileList, identifiersFileList
	 * All these references and variables are private
	 ***/
	
	private static LoadConfigProperties loadConfigProperties;
	private static LoadProperties loadProperties = new LoadProperties();
	private static String sBrowserType;
	private static final String sTestDataFolderName = "testdata";
	private static final String sIdentifiersFolderName = "identifiers";
	protected ClassLoader classLoader;
	private HashSet<String> testDataFileList;
	private HashSet<String> identifiersFileList;
	private WebDriverWait wait;
	private Actions builder;

	/**
	 * Will check all required files are present in the resource folder or not.
	 * <p>
	 * Creates the driver and initializes to give browser. If browser not provided creates Firefox driver by default.
	 * <p>
	 * @author   Vijay Kumar Reddy Gandra 
	 * @author	 Venkat Ramana Reddy Parine
	 * <p>
	 * @since	1.0
	 */
	
    public Autonium() {
	    	
    	classLoader = getClass().getClassLoader();
    	/***
    	 * This condition is to verify config.properties file presence in the resources folder.
    	 * If config.properties file not present in the resources folder. Then throwing an error and stopping the test execution.
    	 ***/
    	if(classLoader.getResourceAsStream("config.properties") != null ){
    		Logger.info("Loading properties files...");
    		loadConfigProperties = new LoadConfigProperties(classLoader);
    		loadPropertiesFolders(sTestDataFolderName);
    		loadPropertiesFolders(sIdentifiersFolderName);
    		initialize();
    	}
    	else { 
    		Logger.error("Config Properties file is not present in resources folder or there may be an issue in naming it."
    				+ "\nAdd Config properties file in resources folder. File Name should be as below\n 1. config.properties");
    	}
	}
    
	/**
	 * Creating the WebDriver with given browser. If user didn't provided any browser then FirefoxDriver will created.   
	 * <p>
	 * Creates the driver and initializes it. 
	 * <p>    
	 * @author   Vijay Kumar Reddy Gandra
	 * <p> 
	 * @since	1.0
	 */
    
    private void initialize() {
    	
    	/***
    	 * Getting browser type from config.properties 
    	 ***/
    	sBrowserType = BrowserTypeExtended.getBrowserType(loadConfigProperties.browserType.trim());
		
    	/***
    	 * checking whether user provided supporting browser or not. Throwing an error and stopping test execution if user provided
    	 * unsupported browser. 
    	 ***/
    	
		if (sBrowserType.equals(BrowserTypeExtended.OTHER))
		{
			Logger.error(BrowserTypeExtended.sUnsuppportedBrowserErrorMsg);
		}
		
		else if(sBrowserType.equals(BrowserTypeExtended.HTMLUNIT)) {
			Logger.warning("Unfortunatly, we are not supporting HTMLUNIT browser at this point. Please try below browsers.");
			Logger.error(BrowserTypeExtended.sUnsuppportedBrowserErrorMsg);
		}
		
		/***
		 * Creating Webdriver for Firefox browser
		 ***/
		
		if(loadConfigProperties.seleniumServer == null || loadConfigProperties.seleniumServer.isEmpty()) {
			if(sBrowserType.equals(BrowserTypeExtended.FIREFOX)) {
				Logger.info("Opening Firefox Browser...");
				FirefoxProfile fProfile = new FirefoxProfile();
				fProfile.setAcceptUntrustedCertificates(true);
				autoniumDriver = new FirefoxDriver(fProfile);
			}
			
			/***
			 * Creating Webdriver for Chrome browser
			 ***/
			
			else if(sBrowserType.equals(BrowserTypeExtended.CHROME)) {
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				//System.setProperty("webdriver.chrome.driver","Chrome driver path");
				Logger.info("Opening Google Chrome Browser...");
				autoniumDriver = (ChromeDriver) new ChromeDriver(capabilities);
			}
			
			/***
			 * Creating Webdriver for Internet Explorer browser
			 ***/
			
			else if(sBrowserType.equals(BrowserTypeExtended.IEXPLORE)) {
				new DesiredCapabilities();
				DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				capabilities.setCapability("ignore-certificate",true);
				Logger.info("Opening IE Browser...");
				autoniumDriver = (InternetExplorerDriver) new InternetExplorerDriver(capabilities);
			}

			/***
			 * Creating Webdriver for Safari browser
			 ***/
			
			else if(sBrowserType.equals(BrowserTypeExtended.SAFARI)){
				new DesiredCapabilities();
				DesiredCapabilities capabilities = DesiredCapabilities.safari();
				capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				autoniumDriver = new SafariDriver();
			}
			autoniumDriver.get(loadConfigProperties.baseurl);
		}
		else {
			DesiredCapabilities capabilities = null;
			if(sBrowserType.equals(BrowserTypeExtended.FIREFOX)) {
				capabilities = DesiredCapabilities.firefox();
				Logger.info("Opening Firefox Browser...");
				FirefoxProfile fProfile = new FirefoxProfile();
				fProfile.setAcceptUntrustedCertificates(true);
				capabilities.setCapability(FirefoxDriver.PROFILE, fProfile);
			}
			
			/***
			 * Creating Webdriver for Chrome browser
			 ***/
			
			else if(sBrowserType.equals(BrowserTypeExtended.CHROME)) {
				capabilities = DesiredCapabilities.chrome();
				capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				//System.setProperty("webdriver.chrome.driver","Chrome driver path");
				Logger.info("Opening Google Chrome Browser...");
			}
			
			/***
			 * Creating Webdriver for Internet Explorer browser
			 ***/
			
			else if(sBrowserType.equals(BrowserTypeExtended.IEXPLORE)) {
				capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				capabilities.setCapability("ignore-certificate",true);
				Logger.info("Opening IE Browser...");
			}

			/***
			 * Creating Webdriver for Safari browser
			 ***/
			
			else if(sBrowserType.equals(BrowserTypeExtended.SAFARI)){
				capabilities = DesiredCapabilities.safari();
				capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			}
			
			try {
				autoniumDriver = new RemoteWebDriver(new URL("http://"+loadConfigProperties.seleniumServer+":"+loadConfigProperties.seleniumServerPort+"/wd/hub/"), capabilities);
			} catch (MalformedURLException e) {e.printStackTrace();}
			catch (UnreachableBrowserException e){
				Logger.error("Not able to load Browser in http://"+loadConfigProperties.seleniumServer+":"+loadConfigProperties.seleniumServerPort+"/wd/hub/  \n\t\t\t  Cross check the Selenium Server and Port details in config.properties file.");}
			autoniumDriver.get(loadConfigProperties.baseurl);
		}
		actionBuilder();
	}
    
	 /**
	 * Returns true or false depends on file existence in provided folder. 
	 * <p>
	 * @param    sFolder, sFileName 
	 * <p>
	 * @return 	 Returns true if provided file is present in the given folder. Otherwise returns false. 
	 * <p>
	 * @author   Vijay Kumar Reddy Gandra 
	 * <p>
	 * @since    1.0
	 */
    
    private boolean verifyPropertyFileExistence(String sFolder, String sFileName) {
    	
    	/**
    	 * Checking given folder is TestData folder or not.
    	 */
    	
    	if(sFolder.equalsIgnoreCase(sTestDataFolderName)){
    		if(testDataFileList.contains(sFileName)) return true;
    		else Logger.error(sFileName+" is not present in "+ sFolder +" Folder. \n Make sure that all test data files should be in "+ sFolder +"  Folder.");
    	}
    	else {
    		if(identifiersFileList.contains(sFileName)) return true;
    		else Logger.error(sFileName+" is not present in "+ sFolder +"  Folder. \n Make sure that all identifiers files should be in "+ sFolder +"  Folder.");
    	}
		return false;
	}
    
    /**
	 * Generates Properties files list from test data and identifiers folders from generated jar file. 
	 * <p>
	 * @param    sFolderName 
	 * <p>
	 * @author   Vijay Kumar Reddy Gandra 
	 * <p>
	 * @since	1.0
	 */
    
    private void loadPropertiesFolders(String sFolderName) {
    	
    	/**
    	 * Checking given folder is TestData folder or not.
    	 */
    	
    	if(sFolderName.equalsIgnoreCase(sTestDataFolderName)) {
	    	
    		File folder = null;
			
    		/**
    		 * Reading the folder and converting it to URI
    		 */
    		
    		try {
				folder = new File(classLoader.getSystemResource(sFolderName+"/").toURI());
			} catch (Exception e) {
				Logger.warning("There is an Exception in loading "+ sFolderName +" folder.");
			}
			
    		/**
    		 * Checking folder presence, if folder is not present printing a warning message.
    		 */
    		
			if(folder == null) Logger.warning(sFolderName +" folder is not present in resources. Please add "+ sFolderName +" folder and files for better experience.");
			
			/**
			 * If folder is present then getting the files list in the folder
			 */
			
			else {
			
				File[] allfilesList = folder.listFiles();
				
				if(allfilesList.equals(null)) {
					Logger.error("There may be an error with "+ sFolderName +"  folder in src/main/resources folder. \nYou should create a "+ sFolderName +"  folder under src/main/resources/");
				}
				if(allfilesList.length == 0) {
					Logger.warning("There are no files in "+ sFolderName +"  folder");
				}
				else {
					testDataFileList = propertiesFileList(allfilesList);
				}
			}
    	}
    	else {
    		
    		File folder = null;
			
    		/**
    		 * Reading the folder and converting it to URI
    		 */
    		
    		try {
				folder = new File(classLoader.getSystemResource(sFolderName+"/").toURI());
			} catch (Exception e) {
				Logger.warning("There is an Exception in loading "+ sFolderName +" folder.");
			}
			
			/**
    		 * Checking folder presence, if folder is not present printing a warning message.
    		 */
			
			if(folder == null) Logger.warning(sFolderName +" folder is not present in resources. Please add "+ sFolderName +" folder and files for better experience.");
			
			/**
			 * If folder is present then getting the files list in the folder
			 */
			
			else {
				
				File[] allfilesList = folder.listFiles();
				
				if(allfilesList.equals(null)) {
					Logger.error("There may be an error with "+ sFolderName +"  folder in src/main/resources folder. \nYou should create a "+ sFolderName +"  folder under src/main/resources/");
				}
				if(allfilesList.length == 0) {
					Logger.warning("There are no files in the "+ sFolderName +"  folder");
				}
				else {
					identifiersFileList= propertiesFileList(allfilesList);
				}
			}
    	}
    }
    
	/**
	 * Returns Hashset with all properties files in the given list from testdata or identifiers folder 
	 * <p>
	 * @param    allfilesList 
	 * <p>
	 * @return 	 Returns HashSet with all properties files. 
	 * <p>
	 * @author   Vijay Kumar Reddy Gandra 
	 * <p>
	 * @since    1.0
	 */

	private HashSet<String> propertiesFileList(File[] allfilesList) {
		
		HashSet<String> results = new HashSet<String>();
		for (File file : allfilesList) {
		    if (file.isFile()) {
		        results.add(file.getName());
		    }
		}
		return results;
	}
	
	/**
	 * Returns value of given property (key) from environment file present in "<b>src/main/resources/testdata</b>" folder. 
	 * <p>
	 * @param    propertyName
	 * 					- Key in properties file.  
	 * <p>
	 * 		Properties are key-value pairs. This method takes key as argument and returns value from environment properties file in testdata folder. 
	 *  <p>
	 * @see      #getIdentifierData(String, String) 
	 * 
	 * @return 	 Returns propertyValue from given file and key. 
	 * <p>
	 * @author   Vijay Kumar Reddy Gandra
	 * @author   Venkat Ramana Reddy Parine 
	 * <p>
	 * @since    1.0
	 */
    
	  public String getTestData(String propertyName) {
	    	
	    	/***
	    	 * Preparing testdata file name from given String by appending .properties to method argument.
	    	 ***/
	    	String sFileName = getEnvironmentVariable()+".properties";
	    	
	    	/***
	    	 * Verifying whether given file is present in the src/main/resources/testdata folder or not.
	    	 ***/
	    	verifyPropertyFileExistence(sTestDataFolderName,sFileName);
	    	return loadProperties.getPropertFromFile(classLoader, sFileName, sTestDataFolderName, propertyName);
    }
    
	/**
	 * Returns value of given property (key) from given identifiers file present in "<b>src/main/resources/identifiers</b>" folder.
	 * <p>
	 * @param    fileName
	 * 				- File from which property to read. 
	 * 
	 * @param    propertyName
	 * 				- Key in properties file. 
	 * <p>
	 * 			 Properties are key-value pairs. This method takes filename and key as arguments and returns value from given properties file in identifiers folder.
	 * <p>
	 * @see      #getTestData(String) 
	 * 
	 * @return 	 Returns value of given property.
	 * <p>
	 * @author   Vijay Kumar Reddy Gandra 
	 * <p>
	 * @since    1.0
	 */
    
    public String getIdentifierData(String fileName, String propertyName) {
    	   	
    	/***
    	 * Preparing testdata file name from given String by appending .properties to method argument.
    	 ***/
    	String sFileName = fileName+".properties";
    	
    	/***
    	 * Verifying whether given file is present in the src/main/resources/identifiers folder or not.
    	 ***/
    	verifyPropertyFileExistence(sIdentifiersFolderName,sFileName);
    	return loadProperties.getPropertFromFile(classLoader, sFileName, sIdentifiersFolderName, propertyName);
    }

    /**
	 * Returns given identifier type 
	 * <p>
	 * @param    identifier
	 * 				- Element Locator 
	 * <p>			 
	 * @return 	 Returns identifier type 
	 * <p>
	 * @author   Vijay Kumar Reddy Gandra 
	 * <p>
	 * @since    1.0
	 */
    
    private By getIdentifierType(String identifier) {
    	
    	if (identifier.startsWith("/"))
			return By.ByXPath.xpath(identifier);
    	else if(identifier.startsWith("xpath:"))
    		return By.ByXPath.xpath(identifier.substring(6));
    	else if(identifier.toLowerCase().startsWith("id:"))
			return By.id(identifier.substring(3));
		else if (identifier.toLowerCase().startsWith("cn:"))
			return By.className(identifier.substring(3));
    	else if (identifier.toLowerCase().startsWith("css:"))
			return By.ByCssSelector.cssSelector(identifier.substring(4));
    	else if (identifier.toLowerCase().startsWith("name:"))
			return By.name(identifier.substring(5));
    	else if (identifier.toLowerCase().startsWith("lt:"))
			return By.ByLinkText.linkText(identifier.substring(3));
		else if (identifier.toLowerCase().startsWith("plt:"))
			return By.partialLinkText(identifier.substring(4));
		else if (identifier.toLowerCase().startsWith("tname:"))
			return By.tagName(identifier.substring(6));
		else {
			if(identifier.contains(":"))
				Logger.UnsuppportedIdentifierErrorMsg(identifier.substring(0, identifier.indexOf(":")));
			else
				Logger.UnsuppportedIdentifierErrorMsg(identifier);
		}
		return By.id(identifier);
	}
    
    /**
	 * Returns WebElement for given Identifier. Fails if element not found.
	 * <p>
	 * @param    identifier
	 * 				-  Element Locator 
	 * 			Allowed identifiers for this method are : 
	 * 			<b>id:</b>    Element Id; 
	 * 			<b>cn:</b>    Element Class Name; 
	 * 			<b>css:</b>   Element CSS Selector; 
	 * 			<b>name:</b>  Element name; 
	 *  		<b>lt:</b>    Element Link Text; 
	 *    		<b>plt:</b>   Element Partial Link Text; 
	 *      	<b>tname:</b> Element Tag Name; 
	 *  		<b>xpath:</b> Element XPath. Also Element xpath can be used directly without "xpath:" 
	 *  		Eg: <b>id</b> - driver.findElement("id:username");<b>xpath</b> - driver.findElement("//input[@id='username']");<b>xpath</b> - driver.findElement("xpath://input[@id='username']");<b>cn</b> - driver.findElement("cn:login");
	 *  		OR Add all identifiers in identifiers file then read from the file using {@link #getIdentifierData(String, String)} method.   
	 * <p>
	 * @see     #findElements(String)
	 * @see		#getIdentifierData(String, String) 
	 * 
	 * @return 	 Returns WebElement. 
	 * <p>
	 * @author   Vijay Kumar Reddy Gandra 
	 * <p>
	 * @since    1.0
	 */
    
    public WebElement findElement(String identifier) {
    	return findElement(getIdentifierType(identifier));
    }
    
    private WebElement findElement(By identifier){
    	WebElement we = null;
    	Logger.info("Finding Element : "+ identifier);
	
		try { 
			we = autoniumDriver.findElement(identifier);
		}
		catch(Exception e){}
		
		if(we == null) 	Logger.error("Element Not Found : "+ identifier);
    	return we;
    }
    
    /**
	 * Returns List of WebElements for given Identifier. Fails if element not found.
	 * <p>
	 * @param    identifier
	 * 				-  Element Locator 
	 * 			Allowed identifiers for this method are : 
	 * 			<b>id:</b>    Element Id; 
	 * 			<b>cn:</b>    Element Class Name; 
	 * 			<b>css:</b>   Element CSS Selector; 
	 * 			<b>name:</b>  Element name; 
	 *  		<b>lt:</b>    Element Link Text; 
	 *    		<b>plt:</b>   Element Partial Link Text; 
	 *      	<b>tname:</b> Element Tag Name; 
	 *  		<b>xpath:</b> Element XPath. Also Element xpath can be used directly without "xpath:" 
	 *  		Eg: <b>id</b> - driver.findElement("id:username");<b>xpath</b> - driver.findElement("//input[@id='username']");<b>xpath</b> - driver.findElement("xpath://input[@id='username']");<b>cn</b> - driver.findElement("cn:login");
	 *  		OR Add all identifiers in identifiers file then read from the file using {@link #getIdentifierData(String, String)} method.   
	 * <p>
	 * @see     #findElement(String)
	 * @see 	#getIdentifierData(String, String) 
	 * 
	 * @return 	 Returns WebElements list. 
	 * <p>
	 * @author   Vijay Kumar Reddy Gandra 
	 * <p>
	 * @since    1.0
	 */
    
    public List<WebElement> findElements(String identifier) {
    	return findElements(getIdentifierType(identifier));
    }
    
    private List<WebElement> findElements(By identifier){
    	List<WebElement> we = null;
    	Logger.info("Finding Element List : "+ identifier );
	
		try { 
			we = autoniumDriver.findElements(identifier);
		}
		catch(Exception e){}
		
		if(we == null) 	Logger.error("Element List Not Found : "+ identifier);
    	return we;
    }
    
    /**
	 * Waits until element specified with identifier appears on current page. Fails if timeout expires before the element appears. 
	 * <p>
	 * @param    identifier
	 * 				-  Element Locator 
	 * 			Allowed identifiers for this method are : 
	 * 			<b>id:</b>    Element Id; 
	 * 			<b>cn:</b>    Element Class Name; 
	 * 			<b>css:</b>   Element CSS Selector; 
	 * 			<b>name:</b>  Element name; 
	 *  		<b>lt:</b>    Element Link Text; 
	 *    		<b>plt:</b>   Element Partial Link Text; 
	 *      	<b>tname:</b> Element Tag Name; 
	 *  		<b>xpath:</b> Element XPath. Also Element xpath can be used directly without "xpath:" 
	 *  		Eg: <b>id</b> - driver.findElement("id:username");<b>xpath</b> - driver.findElement("//input[@id='username']");<b>xpath</b> - driver.findElement("xpath://input[@id='username']");<b>cn</b> - driver.findElement("cn:login");
	 *  		OR Add all identifiers in identifiers file then read from the file using {@link #getIdentifierData(String, String)} method.   
	 *
	 *@param	timeOutInSeconds
	 *				- Time in seconds to wait for given identifier. 
	 * <p>
	 * @see      #waitForElementVisible(String, int) 
	 * 
	 * @author   Vijay Kumar Reddy Gandra 
	 * <p>
	 * @since    1.0
	 */
    
    public void waitForElementPresence(String identifier, int timeOutInSeconds) {
    	
    	WebElement we = null;
    	wait = new WebDriverWait(autoniumDriver, timeOutInSeconds);
    	Logger.info("Waiting for Element Presence : "+ identifier);
    	
    		try { 
    			we = wait.until(ExpectedConditions.presenceOfElementLocated(getIdentifierType(identifier)));
    		} 
    		catch(Exception e){}
    		
    		if(we == null) 	Logger.error("Element " + identifier + " is Not Found in " + timeOutInSeconds + " Seconds");
    }
    
     /**
	 * Waits until element specified with identifier is visible. Fails if timeout expires before the element is visible. 
	 * <p>
	 * @param    identifier
	 * 				-  Element Locator 
	 * 			Allowed identifiers for this method are : 
	 * 			<b>id:</b>    Element Id; 
	 * 			<b>cn:</b>    Element Class Name; 
	 * 			<b>css:</b>   Element CSS Selector; 
	 * 			<b>name:</b>  Element name; 
	 *  		<b>lt:</b>    Element Link Text; 
	 *    		<b>plt:</b>   Element Partial Link Text; 
	 *      	<b>tname:</b> Element Tag Name; 
	 *  		<b>xpath:</b> Element XPath. Also Element xpath can be used directly without "xpath:" 
	 *  		Eg: <b>id</b> - driver.findElement("id:username");<b>xpath</b> - driver.findElement("//input[@id='username']");<b>xpath</b> - driver.findElement("xpath://input[@id='username']");<b>cn</b> - driver.findElement("cn:login");
	 *  		OR Add all identifiers in identifiers file then read from the file using {@link #getIdentifierData(String, String)} method.   
	 *
	 *@param	timeOutInSeconds
	 *				- Time in seconds to wait for given identifier. 
	 * <p>
	 * @see      #waitForElementPresence(String, int) 
	 * 
	 * @author   Vijay Kumar Reddy Gandra 
	 * <p>
	 * @since    1.0
	 */    
    public void waitForElementVisible(String identifier, int timeOutInSeconds) {
    	
    	WebElement we = null;
    	wait = new WebDriverWait(autoniumDriver, timeOutInSeconds);
    	Logger.info("Waiting for Element Visibility : "+ identifier);
    
    		try {
    			we = wait.until(ExpectedConditions.visibilityOfElementLocated(getIdentifierType(identifier)));
    		} 
    		catch(Exception e){}
    		
    		if(we == null) 	Logger.error("Element " + identifier + " is Not Visible in " + timeOutInSeconds + " Seconds");
    }
    
    /**
     * Selects value from list identified by identifier. Fails if identifier not found or Unsupported select type is used. 
     * <p>
     * @param    identifier
	 * 				-  Element Locator 
	 * 			Allowed identifiers for this method are : 
	 * 			<b>id:</b>    Element Id; 
	 * 			<b>cn:</b>    Element Class Name; 
	 * 			<b>css:</b>   Element CSS Selector; 
	 * 			<b>name:</b>  Element name; 
	 *  		<b>lt:</b>    Element Link Text; 
	 *    		<b>plt:</b>   Element Partial Link Text; 
	 *      	<b>tname:</b> Element Tag Name; 
	 *  		<b>xpath:</b> Element XPath. Also Element xpath can be used directly without "xpath:"    
	 *@param	selectValue
	 *			  - Select Type with select value 
	 *			Allowed select type for this method are : 
	 * 			<b>value:</b>	Select By Value;
	 * 			<b>vtext:</b>	Select By Visible Text;
	 * 			<b>index:</b>	Select By Index; 
	 *			Eg: <b>index:</b> - driver.selectFromDropDown("id:continents","index:1");<b>vtext:</b> - driver.selectFromDropDown("id:continents","vtext:Europe");<b>value:</b> - driver.selectFromDropDown("id:continents","value:Asia"); 
	 *  		OR Add all identifiers in identifiers file then read from the file using {@link #getIdentifierData(String, String)} method.
	 *  		Add all select types (test data) in environment file then read from the file using {@link #getTestData(String)} method.
	 * <p>
	 * @see     #deSelectFromDropDown(String, String) 
     * @author   Venkat Ramana Reddy Parine 
     * <p>
     * @since    1.0
     */

    public void selectFromDropDown(String identifier, String selectValue) {
    	
    	Select se = new Select(findElement(identifier));
    	String selectValue_temp;
    	
    	if(selectValue.startsWith("value:")) {
    		selectValue_temp = selectValue.substring(6);
    		Logger.info("Selecting "+ selectValue_temp +" By Value in "+identifier);
    		try { se.selectByValue(selectValue_temp);}
    		catch(Exception e){Logger.error("Cannot locate option with value: "+selectValue_temp+ " in " +identifier);}
    	}
    	
    	else if(selectValue.startsWith("vtext:")) {
    		selectValue_temp = selectValue.substring(6);
    		Logger.info("Selecting "+ selectValue_temp +" By Visible Text in "+identifier);
    		try { se.selectByVisibleText(selectValue_temp);}
    		catch(Exception e){Logger.error("Cannot locate option with visible text: "+selectValue_temp+ " in " +identifier);}
    	}
    	
    	else if(selectValue.startsWith("index:")) {
    		selectValue_temp = selectValue.substring(6);
    		Logger.info("Selecting "+ selectValue_temp +" By Index in "+identifier);
    		try { se.selectByIndex(Integer.parseInt(selectValue_temp));}
    		catch(Exception e){Logger.error("Cannot locate option with index: "+selectValue_temp+ " in " +identifier);}
    	}
    	
    	else{
    		Logger.UnsuppportedSelectorErrorMsg(selectValue);
    	}

    }
    
    /**
     * Deselects value from list identified by identifier. Fails if identifier not found or Unsupported deselect type is used. 
     * <p>
     * @param    identifier
	 * 				-  Element Locator 
	 * 			Allowed identifiers for this method are : 
	 * 			<b>id:</b>    Element Id; 
	 * 			<b>cn:</b>    Element Class Name; 
	 * 			<b>css:</b>   Element CSS Selector; 
	 * 			<b>name:</b>  Element name; 
	 *  		<b>lt:</b>    Element Link Text; 
	 *    		<b>plt:</b>   Element Partial Link Text; 
	 *      	<b>tname:</b> Element Tag Name; 
	 *  		<b>xpath:</b> Element XPath. Also Element xpath can be used directly without "xpath:"    
	 * <p>
	 * @param	deSelectValue
	 *			  - deSelect Type with select value 
	 * 			Allowed deselect type for this method are : 
	 * 			<b>value:</b>	Deselect By Value;
	 * 			<b>vtext:</b>	Deselect By Visible Text;
	 * 			<b>index:</b>	Deselect By Index; 
	 *			Eg: <b>index:</b> - driver.deSelectFromDropDown("id:continents","index:1");<b>vtext:</b> - driver.deSelectFromDropDown("id:continents","vtext:Europe");<b>value:</b> - driver.deSelectFromDropDown("id:continents","value:Asia"); 
	 *  		OR Add all identifiers in identifiers file then read from the file using {@link #getIdentifierData(String, String)} method.
	 *  		Add all deselect types (test data) in environment file then read from the file using {@link #getTestData(String)} method.
	 * <p>
	 * @see     #selectFromDropDown(String, String)
     * @author   Venkat Ramana Reddy Parine 
     * <p>
     * @since    1.0
     */
    
    public void deSelectFromDropDown(String identifier, String deSelectValue){
        
    	Select se = new Select(findElement(identifier));
    	String deSelectValue_temp;
    	
    	if(deSelectValue.startsWith("value:")) {
    		deSelectValue_temp = deSelectValue.substring(6);
    		Logger.info("Deselecting "+ deSelectValue_temp +" By Value in "+identifier);
    		try { se.deselectByValue(deSelectValue_temp);}
    		catch(Exception e){Logger.error("Given " + deSelectValue + " is not found in "+identifier);}
    	}
    	
    	else if(deSelectValue.startsWith("vtext:")) {
    		deSelectValue_temp = deSelectValue.substring(6);
    		Logger.info("Deselecting "+ deSelectValue_temp +" By Visible Text in "+identifier);
    		try { se.deselectByVisibleText(deSelectValue_temp);}
    		catch(Exception e){Logger.error("Given " + deSelectValue + " is not found in "+identifier);}
    	}
    	
    	else if(deSelectValue.startsWith("index:")) {
    		deSelectValue_temp = deSelectValue.substring(6);
    		Logger.info("Deselecting "+ deSelectValue_temp +" By Index in "+identifier);
    		try { se.deselectByIndex(Integer.parseInt(deSelectValue_temp));}
    		catch(Exception e){ Logger.error("Given "+ deSelectValue +" is not found in "+identifier);}
    	}
    	
    	else{
    		Logger.UnsuppportedSelectorErrorMsg(deSelectValue);
    	}

}
    /**
     * Returns environment variable value from config file. 
     * <p>
     * @see      #setEnvironmentVariable(String) 
     * @return 	 Returns Environment variable. 
     * <p>
     * @author   Venkat Ramana Reddy Parine 
     * <p>
     * @since    1.0
     */     
    public String getEnvironmentVariable(){
    	
    	return loadConfigProperties.environment;
    	
    }
    
    /**
     * Sets environment variable. Remaining test execution will be done in this environment. 
     *  <p>
     * @param    newEnvironmentValue
     * 				- This value will be set as environment.
     * <p>
     * @see      #getEnvironmentVariable() 
     * @author   Venkat Ramana Reddy Parine 
     * <p>
     * @since    1.0
     
     * For Time being making this method as private [Not accessible to user]
     */   
    
    private void setEnvironmentVariable(String newEnvironmentValue){
    	
    	setValueToConfigFile("Environment", newEnvironmentValue);
    	
    }
    
    /**
     * Sets value to config file variables
     * <p>
     * @param    Key
     * @param	 Value 
     * <p>
     * @see      #setEnvironmentVariable(String) 
     * @author   Venkat Ramana Reddy Parine 
     * <p>
     * @since    1.0
     */   
    private void setValueToConfigFile(String Key, String Value){
    	
    	if(Key.equals("Environment")){
    		Logger.info("Updating "+Key+" from "+loadConfigProperties.environment+" to "+Value);
    		loadConfigProperties.environment = Value;
    	}
    }
       
    /**
	 * Returns HTTP Response status code for given URL. Returns <b>0</b> as response code if any exception occurs.
	 * <p>
	 * @param	urlString
	 *				- URL in string format for which Status code is required. Eg: http://www.autonium.wordpress.com
	 * @return Returns Status Code for the given URL. Returns <b>0</b> as response code if any exception occurs.
	 * <p>
	 * 			If URL not provided in given format then throws MalformedURLException warning. 
	 * 			Along with MalformedURLException this method will throw a warning for SocketTimeoutException and IOException.
	 * <p>  
	 * 			Currently we are unable to capture below response codes:<br>
	 * 				301,302,303,305 and 307.
	 * <p>
	 * 			<b>Note:</b> Capturing 301 response code when redirecting from <b>http to https</b> or <b>https to http</b>. 
	 * 			Capturing 302 response code also in some cases but there is no consistency.
	 * <p>
	 * @author   Vijay Kumar Reddy Gandra 
	 * <p>
	 * @since    1.1
	 */    
    
    public int getHttpStatusCode(String urlString) {
    	
    	HttpURLConnection connection;
    	URL url;
    	int statusCode = 0;
    	try{
    		url = new URL(urlString);
    		connection = (HttpURLConnection) url.openConnection();
    		statusCode = connection.getResponseCode();
    	}
    	catch(MalformedURLException e) {
    		Logger.warning("java.net.MalformedURLException for the URL : "+ urlString);
    	}
    	catch(SocketTimeoutException e) {
    		Logger.warning("java.net.SocketTimeoutException the given URL : "+ urlString);
    	} catch (IOException e) {
    		Logger.warning("java.io.IOException for the URL : "+ urlString);
		}
		return statusCode;
    }
    
    /**
     * Initializes builder variable of Actions class. This method when called attaches driver to builder variable.
     * @author Venkat Ramana Reddy Parine
     * @since 1.2
     */
    
    private void actionBuilder(){
    	builder = new Actions(autoniumDriver);
    }
    
    /**
     * Hovers mouse over given element
     * <p>
     * @param    identifier - Identifier format is same as specified for Find Element method.
     * @see #findElement(String)
     * @author   Venkat Ramana Reddy Parine 
     * @since    1.2
     */  
    
    public void mouseHover(String identifier){
    	builder.moveToElement(findElement(identifier)).perform();
    }
    
    /**
     * Drag and drop can be done by using this method. When invoked, this method drags an element from source and drops the element at target.
     * @param source - Source parameter format is same as specified for Find element method.
     * @param target - Target parameter format is same as specified for Find element method.
     * @author Venkat Ramana Reddy Parine
     * @see #findElement(String)
     * @since 1.2
     */
    public void dragAndDrop(String source, String target){
    	builder.dragAndDrop(findElement(source), findElement(target)).perform();
    }
    
    /**
     * Double click can be achieved by using this method. When invoked, this method performs double click on identifier.
     * @param identifier - Identifier parameter format is same as specified for find element method.
     * @author Venkat Ramana Reddy Parine
     * @see #findElement(String)
     * @since 1.2
     */
    public void doubleClick(String identifier){
    	builder.doubleClick(findElement(identifier)).perform();
    }
    
    /**
     * Captures screenshot of current page.
     * <p>
     * This method captures screenshot of current page and by default stores in Screenshots folder of workspace. Saves screenshot with .png format.
     * <p>
     * Throws appropriate error if Screenshot cannot be captured or Screenshots folder cannot be created.
     * @since 1.2
     * @author Venkat Ramana Reddy Parine
     * @see #takeScreenshot(String)
     * @see #takeScreenshot(String, String)
     */
    public void takeScreenshot(){
    	
    	try {
			File file = new File("Screenshots");
			
			if (!file.exists()) {
				file.mkdir();
				Logger.info("Created Directory - " + file+" for storing screenshots.");
			}
			
			String screenShot = "screenshot_"+Logger.getFormattedDate("MM-dd-yyyy_hh.mm.ss")+".png";
			
			try{
			File screenshotFile = ((TakesScreenshot)autoniumDriver).getScreenshotAs(OutputType.FILE);
			File targetFile = new File(file,screenShot);
				
			FileUtils.copyFile(screenshotFile, targetFile);
			Logger.info("Screenshot taken now "+screenShot+" is available in Screenshots folder.");
			
			} catch(Exception e){
				Logger.error("An error occured while taking screenshot. Driver might be not available.");
			}
		} catch (Exception e) {
			Logger.error("An error occured while creating directory. Please check the permissions.");
		}
    }
    
    /**
     * This method can be used to Take screenshot with the required file name. 
     * @param fileName - Provide file name with which screenshot should be taken.
     * Screenshots will be stored in Screenshots folder along with .png extension.
     * Appropriate error will be displayed when empty File name is passed to method or File name with extension is passed to method.
     * <p>
     * Examples for file name : Homepage, Login etc.
     * <p>
     * Screenshots will be stored as <b>Homepage.png, Login.png</b>
     * <p>
     * If file name is given as <b>Homepage.jpg</b>, error will be thrown advising user to give file name without extension.
     * @author Venkat Ramana Reddy Parine
     * @since 1.2
     * @see #takeScreenshot()
     * @see #takeScreenshot(String, String)
     */
    
    public void takeScreenshot(String fileName){
    	
    	if(fileName.isEmpty() || fileName==null){
    		Logger.error("Screenshot name cannot be empty. Please provide a name for screenshot.");
    	}
    	
    	else if(fileName.indexOf(".") != -1){
    		Logger.error("Screenshot name should not contain any file extension. Please remove the file extension and try again.");
    	}
    	else{
    		
    		fileName = fileName + ".png";
    		
    	try {
			File file = new File("Screenshots");
			
			if (!file.exists()) {
				file.mkdir();
				Logger.info("Created Directory - " + file+" for storing screenshots.");
			}
			
			try{
			File screenshotFile = ((TakesScreenshot)autoniumDriver).getScreenshotAs(OutputType.FILE);
			File targetFile = new File(file,fileName);
				
			FileUtils.copyFile(screenshotFile, targetFile);
			Logger.info("Screenshot taken now: "+fileName +" is available in Screenshots folder.");
			
			} catch(Exception e){
				Logger.error("An error occured while taking screenshot. Driver might be not available.");
			}
		} catch (Exception e) {
			Logger.error("An error occured while creating directory. Please check the permissions.");
		}
    	}
    }
    
    /**
     * This method takes screenshot and saves it with given file name in given directory path. 
     * Directory will be created in workspace if directory name is given as argument instead of directory path.
     * @param fileName - Valid file name with which screenshot will be saved.
     * <p>
     * Examples for file name : homepage, login. Screenshots will be saved with .png extension by default.
     * @param dirPath - Valid directory path.
     * <p>
     * Examples for directory path : D:\\PaymentImages, E:\\Screenshots\\Images.
     * <p>
     * File name : Homepage, Directory path : SiteImages. Homepage.png will be stored in SiteImages folder of workspace.
     * <p>
     * File name : Login, Directory path : G:\\Images. Login.png will be stored in G:\\Images folder.
     * @author Venkat Ramana Reddy Parine
     * @since 1.2
     * @see #takeScreenshot()
     * @see #takeScreenshot(String)
     */
    public void takeScreenshot(String fileName, String dirPath){
    	if(fileName.isEmpty() || fileName == null || dirPath.isEmpty() || dirPath == null){
    		Logger.error("Screenshot name/directory name cannot be empty. Please provide valid names for Screenshot/Directory.");
    	}
    	
    	else if(fileName.indexOf(".") != -1){
    		Logger.error("Screenshot name should not contain any file extension. Please remove the file extension and try again.");
    	}
    	else{
    		
    		fileName = fileName + ".png";
    	try {
			File file = new File(dirPath);
			
			if (!file.exists()) {
				if(!file.mkdir()){
					Logger.error("Given directory path might not be valid. Choose a valid directory for storing screenshots.");
				}
			}
			
			try{
		    	
			File screenshotFile = ((TakesScreenshot)autoniumDriver).getScreenshotAs(OutputType.FILE);
			File targetFile = new File(file,fileName);
				
			FileUtils.copyFile(screenshotFile, targetFile);
			Logger.info("Screenshot taken now: "+fileName +" is available in "+dirPath+" folder.");
			
			} catch(Exception e){
				Logger.error("An error occured while taking screenshot. Driver might not be available.");
			}
		} catch (Exception e) {
			Logger.error("An error occured while creating directory. Please check the permissions.");
		}
    	}
    }
}

